import asyncio

from fake_useragent import UserAgent
from playwright.async_api import async_playwright

from postgres.utils import get_all_proxies, insert_stock_info
from postgres.connection import connect_postgres, disconnect_postgres

import tracemalloc
import random
import datetime


class Browser:

    async def create_browser(self, pw):
        self.browser = await pw.firefox.launch(headless=True)

    async def check_locator(self, object, selector: str):
        locator = object.locator(selector)

        """
        Иногда локатор не успевает прогрузиться, но такое происходит довольно редко
        Чтобы не ставить статичний длительный sleep на каждый локатор,  
        решил ждать только в том случае, если его, собственно, не видно
        """
        if await locator.count() == 0:
            await asyncio.sleep(10)
            locator = object.locator(selector)

        if await locator.count() == 0:
            raise Exception(f"msg=Wrong locator. Selector={selector}")
        return locator

    async def create_context(self, proxy):
        return await self.browser.new_context(
            user_agent=UserAgent("firefox").random,
            proxy={
                "server": proxy["ip"] + ":" + proxy["port"],
                "username": proxy["login"],
                "password": proxy["password"],
            },
        )

    async def get_all_links(self, context, q: asyncio.Queue) -> None:
        page = await context.new_page()
        page.set_default_timeout(0)

        await page.goto("https://ru.investing.com/equities")
        await asyncio.sleep(random.uniform(0.5, 1))

        index_locator = await self.check_locator(page, "#index-select")
        await index_locator.click()
        await asyncio.sleep(random.uniform(0.5, 1))

        markets_container = await self.check_locator(
            page, "#index-select > div.dropdown_menu__PBQqZ > div > div"
        )

        markets = await self.check_locator(markets_container, "div")
        markets_count = await markets.count()

        # ! (markets_count)
        for i in range(2):
            await markets.nth(i).click()
            await asyncio.sleep(random.uniform(0.5, 1))

            stocks_container = await self.check_locator(
                page,
                "#__next > div.md\:relative.md\:bg-white > div.relative.flex > div.grid.flex-1.grid-cols-1.px-4.pt-5.font-sans-v2.text-\[\#232526\].antialiased.xl\:container.sm\:px-6.md\:grid-cols-\[1fr_72px\].md\:gap-6.md\:px-7.md\:pt-10.md2\:grid-cols-\[1fr_420px\].md2\:gap-8.md2\:px-8.xl\:mx-auto.xl\:gap-10.xl\:px-10 > div.min-w-0 > div:nth-child(4) > div.relative.w-full > div.relative.dynamic-table-v2_dynamic-table-wrapper__fBEvo.dynamic-table-v2_scrollbar-x__R96pe > table > tbody > tr",
            )

            count_of_stocks = await stocks_container.count()
            # ! (count_of_stocks)
            for j in range(3):
                stock_url_locator = await self.check_locator(
                    page,
                    f"#__next > div.md\:relative.md\:bg-white > div.relative.flex > div.grid.flex-1.grid-cols-1.px-4.pt-5.font-sans-v2.text-\[\#232526\].antialiased.xl\:container.sm\:px-6.md\:grid-cols-\[1fr_72px\].md\:gap-6.md\:px-7.md\:pt-10.md2\:grid-cols-\[1fr_420px\].md2\:gap-8.md2\:px-8.xl\:mx-auto.xl\:gap-10.xl\:px-10 > div.min-w-0 > div:nth-child(4) > div.relative.w-full > div.relative.dynamic-table-v2_dynamic-table-wrapper__fBEvo.dynamic-table-v2_scrollbar-x__R96pe > table > tbody > tr:nth-child({j + 1}) > td.datatable-v2_cell__IwP1U.\!h-auto.w-full.py-2.mdMax\:border-r.dynamic-table-v2_col-name__Xhsxv.\!py-2 > div > a",
                )
                stock_url = await stock_url_locator.get_attribute("href")
                q.put_nowait(stock_url)

            await index_locator.click()
            await asyncio.sleep(random.uniform(0.3, 0.6))

        await page.close()

    async def get_one_day(self, line_locator) -> dict:

        ind_content_match = {
            0: "date",
            1: "close",
            2: "open",
            3: "max",
            4: "min",
            5: "volume",
        }

        params = dict()
        params_locator = await self.check_locator(line_locator, "td")

        # Собрал в словарь все значения
        for i in range(6):
            params[ind_content_match[i]] = await params_locator.nth(i).text_content()

        return params

    async def get_one_stock(self, context, stock_url: str) -> list[dict]:
        print(stock_url, ":", datetime.datetime.now())

        page = await context.new_page()
        page.set_default_timeout(0)

        stock_full_url = "https://ru.investing.com/" + stock_url
        try:
            await page.goto(stock_full_url)
        except Exception as ex:
            raise Exception(
                f"msg=Something went wrong while going to stock_url. url={stock_full_url}. ex={ex}"
            )

        await asyncio.sleep(random.uniform(0.5, 1))

        # Прошлые значения
        past_values_locator = await self.check_locator(
            page,
            "#__next > div.md\:relative.md\:bg-white > div.relative.flex > div.grid.flex-1.grid-cols-1.px-4.pt-5.font-sans-v2.text-\[\#232526\].antialiased.xl\:container.sm\:px-6.md\:grid-cols-\[1fr_72px\].md\:gap-6.md\:px-7.md\:pt-10.md2\:grid-cols-\[1fr_420px\].md2\:gap-8.md2\:px-8.xl\:mx-auto.xl\:gap-10.xl\:px-10 > div.min-w-0 > nav > div:nth-child(2) > ul > li:nth-child(3) > a",
        )
        await past_values_locator.click()

        await asyncio.sleep(random.uniform(0.5, 1))

        # Клик по выбору даты
        date_select_locator = await self.check_locator(
            page,
            "#__next > div.md\:relative.md\:bg-white > div.relative.flex > div.grid.flex-1.grid-cols-1.px-4.pt-5.font-sans-v2.text-\[\#232526\].antialiased.xl\:container.sm\:px-6.md\:grid-cols-\[1fr_72px\].md\:gap-6.md\:px-7.md\:pt-10.md2\:grid-cols-\[1fr_420px\].md2\:gap-8.md2\:px-8.xl\:mx-auto.xl\:gap-10.xl\:px-10 > div.min-w-0 > div.mb-4.md\:mb-10 > div.sm\:flex.sm\:items-end.sm\:justify-between > div.relative.flex.items-center.md\:gap-6",
        )
        await date_select_locator.click()

        await asyncio.sleep(random.uniform(0.5, 1))

        """
        # Имеется ограничение в 5000 строчек у таблицы 
        # Следовательно, чтобы гарантированно не терять последние даты и взять как можно больше, 
        # Мы вычисляем текущую дату и вычитаем из нее 7000 дней (так как выходные дни не торговые)
        # В итоге мы получаем чуть больше, чем 4900 дней (из-за других праздников), вроде норм
        """

        # ! 7000
        date = datetime.date.today() - datetime.timedelta(days=100)

        # Установка нужной даты
        date_locator = await self.check_locator(
            page,
            "#__next > div.md\:relative.md\:bg-white > div.relative.flex > div.grid.flex-1.grid-cols-1.px-4.pt-5.font-sans-v2.text-\[\#232526\].antialiased.xl\:container.sm\:px-6.md\:grid-cols-\[1fr_72px\].md\:gap-6.md\:px-7.md\:pt-10.md2\:grid-cols-\[1fr_420px\].md2\:gap-8.md2\:px-8.xl\:mx-auto.xl\:gap-10.xl\:px-10 > div.min-w-0 > div.mb-4.md\:mb-10 > div.sm\:flex.sm\:items-end.sm\:justify-between > div.relative.flex.items-center.md\:gap-6 > div.absolute.right-0.top-\[42px\].z-5.inline-flex.flex-col.items-end.gap-4.rounded.border.border-solid.bg-white.p-4.shadow-secondary > div.flex.items-start.gap-3 > div:nth-child(1) > input",
        )
        await date_locator.fill(str(date))

        await asyncio.sleep(random.uniform(0.5, 1))

        # Принять
        accept_locator = await self.check_locator(
            page,
            "#__next > div.md\:relative.md\:bg-white > div.relative.flex > div.grid.flex-1.grid-cols-1.px-4.pt-5.font-sans-v2.text-\[\#232526\].antialiased.xl\:container.sm\:px-6.md\:grid-cols-\[1fr_72px\].md\:gap-6.md\:px-7.md\:pt-10.md2\:grid-cols-\[1fr_420px\].md2\:gap-8.md2\:px-8.xl\:mx-auto.xl\:gap-10.xl\:px-10 > div.min-w-0 > div.mb-4.md\:mb-10 > div.sm\:flex.sm\:items-end.sm\:justify-between > div.relative.flex.items-center.md\:gap-6 > div.absolute.right-0.top-\[42px\].z-5.inline-flex.flex-col.items-end.gap-4.rounded.border.border-solid.bg-white.p-4.shadow-secondary > div.flex.cursor-pointer.items-center.gap-3.rounded.bg-v2-blue.py-2\.5.pl-4.pr-6.shadow-button.hover\:bg-\[\#116BCC\]",
        )
        await accept_locator.click()

        # !
        await asyncio.sleep(random.randint(12, 15))

        table = await self.check_locator(
            page,
            "#__next > div.md\:relative.md\:bg-white > div.relative.flex > div.grid.flex-1.grid-cols-1.px-4.pt-5.font-sans-v2.text-\[\#232526\].antialiased.xl\:container.sm\:px-6.md\:grid-cols-\[1fr_72px\].md\:gap-6.md\:px-7.md\:pt-10.md2\:grid-cols-\[1fr_420px\].md2\:gap-8.md2\:px-8.xl\:mx-auto.xl\:gap-10.xl\:px-10 > div.min-w-0 > div.mb-4.md\:mb-10 > div.mt-6.flex.flex-col.items-start.overflow-x-auto.p-0.md\:pl-1 > table > tbody",
        )

        lines = await self.check_locator(table, "tr")

        count_of_lines = await lines.count()

        currency_locator = await self.check_locator(
            page,
            "#__next > div.md\:relative.md\:bg-white > div.relative.flex > div.grid.flex-1.grid-cols-1.px-4.pt-5.font-sans-v2.text-\[\#232526\].antialiased.xl\:container.sm\:px-6.md\:grid-cols-\[1fr_72px\].md\:gap-6.md\:px-7.md\:pt-10.md2\:grid-cols-\[1fr_420px\].md2\:gap-8.md2\:px-8.xl\:mx-auto.xl\:gap-10.xl\:px-10 > div.min-w-0 > div.flex.flex-col.gap-6.md\:gap-0 > div.font-sans-v2.md\:flex > div.relative.mb-3\.5.md\:flex-1 > div > div.flex.items-center.gap-1 > div.flex.items-center.pb-0\.5.text-xs\/5.font-normal > span",
        )
        currency = await currency_locator.text_content()

        name_locator = await self.check_locator(
            page,
            "#__next > div.md\:relative.md\:bg-white > div.relative.flex > div.grid.flex-1.grid-cols-1.px-4.pt-5.font-sans-v2.text-\[\#232526\].antialiased.xl\:container.sm\:px-6.md\:grid-cols-\[1fr_72px\].md\:gap-6.md\:px-7.md\:pt-10.md2\:grid-cols-\[1fr_420px\].md2\:gap-8.md2\:px-8.xl\:mx-auto.xl\:gap-10.xl\:px-10 > div.min-w-0 > div.flex.flex-col.gap-6.md\:gap-0 > div.font-sans-v2.md\:flex > div.relative.mb-3\.5.md\:flex-1 > h1",
        )
        name = await name_locator.text_content()
        ticker = name.split("(")[-1][:-1]

        stock_name_locator = await self.check_locator(
            page,
            "#__next > div.md\:relative.md\:bg-white > div.relative.flex > div.grid.flex-1.grid-cols-1.px-4.pt-5.font-sans-v2.text-\[\#232526\].antialiased.xl\:container.sm\:px-6.md\:grid-cols-\[1fr_72px\].md\:gap-6.md\:px-7.md\:pt-10.md2\:grid-cols-\[1fr_420px\].md2\:gap-8.md2\:px-8.xl\:mx-auto.xl\:gap-10.xl\:px-10 > div.min-w-0 > div.flex.flex-col.gap-6.md\:gap-0 > div.font-sans-v2.md\:flex > div.relative.mb-3\.5.md\:flex-1 > div > div.relative-selector-v2_relative-selector___nxqR.flex-shrink.overflow-hidden > div.relative.flex.cursor-pointer.items-center.gap-3.border-b-2.border-transparent.hover\:border-\[\#1256A0\].hover\:text-\[\#1256A0\] > div > span",
        )
        stock_name = await stock_name_locator.text_content()
        stock = []

        for i in range(count_of_lines):
            params = await self.get_one_day(lines.nth(i))
            params["currency"] = currency
            params["ticker"] = ticker
            params["stock_name"] = stock_name
            stock.append(params)

        await page.close()

        return stock


async def worker(b: Browser, q: asyncio.Queue, proxy: dict):
    context = await b.create_context(proxy)
    while not q.empty():
        stock_url = await q.get()
        stock = await b.get_one_stock(context, stock_url)
        await insert_stock_info(stock)


async def main():
    async with async_playwright() as pw:
        q = asyncio.Queue(maxsize=10000)
        b = Browser()

        await connect_postgres()
        proxies = await get_all_proxies()
        random.shuffle(proxies)

        await b.create_browser(pw)
        context = await b.create_context(random.choice(proxies))

        await b.get_all_links(context, q)
        print("all links have been gotten\n")

        await context.close()

        await asyncio.gather(
            *[asyncio.create_task(worker(b, q, proxies[i])) for i in range(3)]
        )

        await disconnect_postgres()


tracemalloc.start()
asyncio.run(main())
