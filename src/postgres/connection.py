from asyncpg import create_pool
from asyncpg.pool import Pool, PoolAcquireContext

from settings import settings


class DataBase:
    db_pool: Pool = None


db = DataBase()


async def connect_postgres():
    try:
        db.db_pool = await create_pool(
            user=settings.postgres_user,
            password=settings.postgres_password,
            host=settings.postgres_host,
            port=settings.postgres_port,
            database=settings.postgres_database_name,
            min_size=0,
            max_size=10,
        )

    except Exception as ex:
        raise Exception(f"msg=Error connecting to postgres. ex={ex}")


async def disconnect_postgres():
    await db.db_pool.close()


async def get_db_connection() -> PoolAcquireContext:
    return db.db_pool
