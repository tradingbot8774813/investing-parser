from postgres.connection import get_db_connection
import datetime

from pandas import DataFrame


async def get_all_proxies() -> list[dict]:
    """Returning all proxies in db"""
    pool = await get_db_connection()

    async with pool.acquire() as connection:
        result = await connection.fetch(
            """
            SELECT * FROM proxy_v6;
            """,
        )

    if not result:
        raise Exception("msg=No vpn config with in db.")

    return list(map(dict, result))


def _format_line_to_tuple(line: dict):
    try:
        volume = float(line["volume"][:-1].replace(",", "."))
        mult = line["volume"][-1]
        match mult:
            case "K":
                volume *= 1_000
            case "M":
                volume *= 1_000_000
            case "B":
                volume *= 1_000_000_000

        return (
            line["ticker"],
            line["stock_name"],
            line["currency"],
            datetime.datetime.strptime(line["date"], "%d.%m.%Y").date(),
            line["close"].replace(",", "."),
            line["open"].replace(",", "."),
            line["max"].replace(",", "."),
            line["min"].replace(",", "."),
            int(volume),
        )
    except Exception as ex:
        raise Exception(f"msg=Error coverting dict to tuple. ex={ex}")


def _split_the_data(data: list[list], count_of_parts):
    length = len(data)
    return [
        data[i * length // count_of_parts : (i + 1) * length // count_of_parts]
        for i in range(count_of_parts)
    ]


async def insert_stock_info(stock: list[dict]) -> None:
    # ! Иногда получается пустой список, хз с чем это связано
    if not stock:
        print(f"msg=Empty stock. Stock: {stock['stock_name']} - {stock['ticker']}")

    # Drop duplicates
    df = DataFrame(stock)
    df = df.drop_duplicates()
    stock = df.to_dict(orient="records")

    stmt = """
            INSERT INTO stock 
            (ticker, stock_name, currency, date, close, open, max, min, volume)
            VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9);
           """
    data = list(map(_format_line_to_tuple, stock))

    count_of_parts = 5

    parts_of_data = _split_the_data(data, count_of_parts)

    pool = await get_db_connection()

    async with pool.acquire() as connection:
        for part in parts_of_data:
            try:
                await connection.executemany(stmt, part)
            except Exception as ex:
                raise Exception(f"msg=Exception when writing to postgres. ex={ex}")
